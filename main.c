
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SQR(x) ((x)*(x)) //square

const int vectors_training = 150;
const int vecLen = 4;
const int row_test = 6;
const int col_test = 10;
const double decayRate = 0.96; // taux de décomposition
const double minAlpha = 0.01;
// Le taux d'apprentissage α au début (t = 0) est de 0,6. 
// c'est-à-dire α (t = 0) = 0,6
double alpha = 0.6;
double *aver = NULL,*min = NULL,*max = NULL;

char result[row_test][col_test];
double w[row_test][col_test][vecLen];
double pattern[vectors_training][vecLen];

void read_data();
void training();
double computeInputTraining(int row, int row_test, int col_test);
//void print_map();
void print_result();
double* init_rand_w(int row_cal_w);
void average_vec(int n);
void min_vec(double k, int Vecnum);
void max_vec(double k, int Vecnum);
void alloc_array_struct(int n);
void update(int row_win, int col_win, int row_data);
void create_neuron_map();

struct vec
{
  char *name;
};
struct vec * array_vec;

void alloc_array_struct(int n)
{
  array_vec=malloc(n*sizeof(struct vec)); // allocation de mémoire dynamique
  int i;
  for(i=0;i<n;i++)
  {
    array_vec[i].name=malloc(20*sizeof(char));
  }
}

int main(){
  alloc_array_struct(vectors_training);
  read_data();
  create_neuron_map();
  //print_map();
  training();
  print_result();
  return 0;
}

void read_data()
{
  FILE * in;

  char *str=malloc(sizeof(char)*500);
  in=fopen("iris.data","r");

  int i,j;
	for(i=0;i<150;i++)
	{
    fscanf(in,"%s",str);
    char *tok=strtok(str,","); // couper la ficelle et commencer - cut string and get first

    for(j=0;j<4;j++)
    { 
      pattern[i][j] = atof(tok); // atof convertir une chaîne en double - convert string to double
      tok=strtok(NULL,",");
    }

    // changez les noms des fleurs en lettres A B C
    if (strcmp(tok, "Iris-setosa") == 0)
      strcpy(array_vec[i].name,"A");
    else if(strcmp(tok,"Iris-versicolor")==0)
      strcpy(array_vec[i].name,"B");
    else
      strcpy(array_vec[i].name,"C");

    //printf (" %s", array_vec[i].name);
	}

	fclose(in);
  free(str);
}

void training(){
  int iterations = 0;
  double dist[row_test][col_test];
  do {
      iterations += 1;
      for(int vecNum = 0; vecNum <= (vectors_training - 1); vecNum++){ 
        // printf ("\n\n row = %d ", vecNum);
        for (int i = 0; i < row_test; i++){
          for (int j = 0; j < col_test; j++){
            dist[i][j] = computeInputTraining(vecNum, i, j); // connaître la somme d'une ligne
            //printf ("\n\n dist[%d][%d] = %f ", i, j, dist[i][j]);
          }

        }
        // begin get dMin
        double min = dist[0][0];
        int row_win = 0;
        int col_win = 0;
        for(int i=0; i<row_test; i++)
        {
          for (int j=0; j<col_test; j++) {
            if (min > dist[i][j]) {
              min = dist[i][j];
              col_win = j;
              row_win = i;
              //printf ("win[%d][%d] = %f", row_win, col_win, min);
            }    
          }
        }
        // end get dMin
        // printf ("\n min = %f", min);

        // Mettez A B C sur le vecteur gagnant
        result[row_win][col_win] = *array_vec[vecNum].name; 
        //printf("\nname[%c]", result[row_win][col_win]);

        // begin mettre à jour le weight avec neuron gagnant
        update(row_win, col_win, vecNum);
        // end update weight avec neuron gagnant  
      } // vecNum
 
      alpha = decayRate * alpha;      
  } while(alpha > minAlpha);
  
  // Ce processus est répété jusqu'à ce que le taux d'apprentissage soit suffisamment petit ou que la convergence de la matrice de weight soit terminée
  printf("Itérations: %d \n\n", iterations);
}

// Recalculer le weight du cluster gagnant et des voisins
void update(int bmu_r, int bmu_c, int row_data)
{
  int nr = 1; // le rayon d'un voisin
  int i,j,x1,x2,y1,y2;//top and bottom

  //printf ("\n\n\n bmu_r = %d ", b_mu->r);

  for(;nr>=0;nr--)
  {
    if(bmu_r-nr<0)
      x1=0;
    else
      x1=bmu_r-nr;
    if(bmu_c-nr<0)
      y1=0;
    else
      y1=bmu_c-nr;

    if(bmu_r+nr>row_test-1)
      x2=row_test-1;
    else
      x2=bmu_r+nr;
    if(bmu_c+nr>col_test-1)
      y2=col_test-1;
    else
      y2=bmu_c+nr;

    for(i=x1;i<=x2;i++)
    {
      //printf ("\n\n i = %d", i);
      for(j=y1;j<=y2;j++)
      {
        //printf ("\n j = %d", j);
        int k;
        //printf("\n\n data = ");
        for(k=0;k<vecLen;k++)
        {
          w[i][j][k]+= alpha*(pattern[row_data][k]-w[i][j][k]);
        }
      }
    }
  }
}

void average_vec(int n)
{
  aver=malloc(vecLen*sizeof(double));
  memset(aver,0,vecLen*sizeof(double));

  int i,j;

  for(i=0;i<vecLen;i++)
  {
    for(j=0;j<n;j++)
      aver[i]+=pattern[j][i];
    aver[i]/=n;
  }
}

void min_vec(double k, int row)
{
  min=malloc(4*sizeof(double));
  int i;
  for(i=0;i<vecLen;i++)
    min[i]=pattern[row][i]-k;
}

void max_vec(double k, int row)
{
  max=malloc(4*sizeof(double));
  int i;
  for(i=0;i<vecLen;i++)
    max[i]=pattern[row][i]+k;
}

// Créer le weight du cluster en max, min et moyenne
double* init_rand_w(int row)
{
  
  average_vec(vectors_training);
  min_vec(0.005, row);
  max_vec(0.005, row);

  int i;
  double k=(double)rand()/RAND_MAX;
  double *tmp_w=malloc(4*sizeof(double));

  for(i=0;i<vecLen;i++) // i: colonne
  {
    tmp_w[i]=k*(max[i]-min[i])+min[i];
  }

  double norm=0.;

  for(i=0;i<vecLen;i++)
  {
    norm+=SQR(tmp_w[i]);
  }
  
  for(i=0;i<vecLen;i++)
  {
      tmp_w[i]/=norm;
  }

  return tmp_w;
}

// Créer le weight du cluster en max, min et moyenne
void create_neuron_map()
{
  int i,j;
  
	for(i=0;i<row_test;i++) // ligne
	{
    //printf ("\n");
		for (j=0;j<col_test;j++) // colonne
		{
      for (int k = 0; k < vecLen; k++) {
        w[i][j][k] = *init_rand_w(i); // Initialisation aléatoire du weights
      }
		}
	}
}
/*
void print_map()
{
  int i,j;
  for(i=0;i<row_test;i++) // ligne
  {
    for(j=0;j<col_test;j++) // colonne
    {
      // printf ("\n\n");
      for (int k = 0; k < vecLen; k++) {
        //printf("%f|",w[i][j][k]);
      }
    }
    //printf("\n");
  }
}
*/
void print_result()
{
  int i,j;
  
  for(i=0;i<row_test;i++) // ligne
  {
    for(j=0;j<col_test;j++) // colonne
    { 
      if (result[i][j] != '\0') {
        printf("%c ", result[i][j]);
      } else {
        printf("%d ", 0);
      }
    }
    printf("\n");
  }
}

// Calcule la distance entre le cluster et les vecteurs d'entrée
double computeInputTraining(int row_data, int row_test, int col_test){
  int i;
  double d = 0.0;
  for(int j = 0; j <= (vecLen - 1); j++){
    d += pow((w[row_test][col_test][j] - pattern[row_data][j]), 2); 
  } // j 
  return sqrt(d); 
}

